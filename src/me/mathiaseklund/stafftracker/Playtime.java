package me.mathiaseklund.stafftracker;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class Playtime {

	public static void storePlaytime(String uuid) {
		Main main = Main.getMain();
		long joined = main.getData().getLong(uuid + ".joined");
		long now = Calendar.getInstance().getTime().getTime();
		long playtime = Math.abs(now - joined) / 1000;
		int playtime_today_total = main.getData().getInt(uuid + ".playtime.today.total");
		playtime_today_total += playtime;
		int playtime_month_total = main.getData().getInt(uuid + ".playtime.month.total");
		playtime_month_total += playtime;

		main.getData().set(uuid + ".playtime.today.total", playtime_today_total);
		main.getData().set(uuid + ".playtime.month.total", playtime_month_total);
		main.getData().set(uuid + ".joined", now);
		main.saveData();
		Util.debug("Total playtime: " + playtime_today_total);
		if (!Main.ess.getUser(UUID.fromString(uuid)).isAfk()) {
			int playtime_today_notafk = main.getData().getInt(uuid + ".playtime.today.notafk");
			playtime_today_notafk += playtime;
			main.getData().set(uuid + ".playtime.today.notafk", playtime_today_notafk);
			int playtime_month_notafk = main.getData().getInt(uuid + ".playtime.month.notafk");
			playtime_month_notafk += playtime;
			main.getData().set(uuid + ".playtime.month.notafk", playtime_month_notafk);
			main.saveData();
		}
	}

	public static void pollHours() {
		Main main = Main.getMain();
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.hasPermission("ontime.track") || p.isOp()) {
				String uuid = p.getUniqueId().toString();
				storePlaytime(uuid);
				storePlaytime(uuid);
				int playtime_today_total, playtime_today_notafk, playtime_month_total, playtime_month_notafk;
				playtime_today_total = main.getData().getInt(uuid + ".playtime.today.total");
				playtime_month_total = main.getData().getInt(uuid + ".playtime.month.total");
				playtime_today_notafk = main.getData().getInt(uuid + ".playtime.today.notafk");
				playtime_month_notafk = main.getData().getInt(uuid + ".playtime.month.notafk");
				int[] times = Util.splitSeconds(new BigDecimal(playtime_month_notafk));
				boolean hour = false;
				Util.debug(times[1] + "");
				if (times[1] == 0) {
					if (times[0] > 0) {
						hour = true;
					}
				}
				if (hour) {
					Util.debug("Played for 1 hour");
					String message = main.getMessages().getString("hour-played");
					NumberFormat formatter = new DecimalFormat("#0.00");
					double hourlypay = main.getConfig().getDouble("hourly-pay");
					message = message
							.replaceAll("%ptt%",
									(formatter.format((double) (playtime_today_total) / 60 / 60)) + " Hours")
							.replaceAll("%ptn%",
									(formatter.format((double) (playtime_today_notafk) / 60 / 60)) + " Hours")
							.replaceAll("%pmt%",
									(formatter.format((double) (playtime_month_total) / 60 / 60)) + " Hours")
							.replaceAll("%pmn%",
									(formatter.format((double) (playtime_month_notafk) / 60 / 60)) + " Hours")
							.replaceAll("%player%", p.getName())
							.replaceAll("%payout%", Util.FormatMoney((hourlypay * times[0])));
					Util.message(p, message);
				}
			}
		}
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
			public void run() {
				pollHours();
			}
		}, 20 * 60);
	}
}
