package me.mathiaseklund.stafftracker;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class CommandOnTime implements CommandExecutor {

	Main main = Main.getMain();

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		if (sender.hasPermission("ontime.view") || sender.isOp()) {
			if (args.length == 1) {
				if (args[0].equalsIgnoreCase("reload")) {
					if (sender.hasPermission("ontime.reload") || sender.isOp()) {
						main.reload();
						Util.message(sender, "&aReloaded StaffTracker");
					}
				} else {
					String t = args[0];
					OfflinePlayer target = Bukkit.getOfflinePlayer(t);
					if (target != null) {
						String uuid = target.getUniqueId().toString();
						if (main.getData().get(uuid) != null) {
							// Playtime.storePlaytime(uuid);
							int playtime_today_total, playtime_today_notafk, playtime_month_total,
									playtime_month_notafk;
							playtime_today_total = main.getData().getInt(uuid + ".playtime.today.total");
							playtime_month_total = main.getData().getInt(uuid + ".playtime.month.total");
							playtime_today_notafk = main.getData().getInt(uuid + ".playtime.today.notafk");
							playtime_month_notafk = main.getData().getInt(uuid + ".playtime.month.notafk");
							int[] times = Util.splitSeconds(new BigDecimal(playtime_month_notafk));
							double hourlypay = main.getConfig().getDouble("hourly-pay");
							NumberFormat formatter = new DecimalFormat("#0.00");
							for (String s : main.getMessages().getStringList("playtime")) {
								Util.message(sender, s
										.replaceAll("%ptt%",
												(formatter.format((double) (playtime_today_total) / 60 / 60))
														+ " Hours")
										.replaceAll("%ptn%",
												(formatter.format((double) (playtime_today_notafk) / 60 / 60))
														+ " Hours")
										.replaceAll("%pmt%",
												(formatter.format((double) (playtime_month_total) / 60 / 60))
														+ " Hours")
										.replaceAll("%pmn%",
												(formatter.format((double) (playtime_month_notafk) / 60 / 60))
														+ " Hours")
										.replaceAll("%player%", target.getName())
										.replaceAll("%payout%", Util.FormatMoney((hourlypay * times[0]))));
							}
						} else {
							Util.message(sender, "&4ERROR:&7 target is not tracked");
						}
					} else {
						Util.message(sender, "&4ERROR:&7 Target not found.");
					}
				}
			} else {
				Util.message(sender, "&4ERROR:&7 Usage: &e/ontime <target>");
			}
		} else {
			Util.message(sender, "&4ERROR:&7 You do not have permission to use this command.");
		}
		return false;
	}

}
