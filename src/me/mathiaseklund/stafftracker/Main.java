package me.mathiaseklund.stafftracker;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import com.earth2me.essentials.Essentials;

public class Main extends JavaPlugin {

	static Main main;
	public static Essentials ess;

	// Files
	File messagesFile;
	FileConfiguration messages;
	File dataFile;
	FileConfiguration data;

	/**
	 * Get instance of Main class
	 * 
	 * @return Main class
	 */
	public static Main getMain() {
		return main;
	}

	/**
	 * Plugin is enabled
	 */
	public void onEnable() {
		main = this;

		LoadYMLFiles();
		RegisterListeners();
		RegisterCommands();
		ess = (Essentials) getServer().getPluginManager().getPlugin("Essentials");
		Playtime.pollHours();
	}

	void reload() {
		this.reloadConfig();
		/**
		 * Messages file
		 */
		messagesFile = new File(getDataFolder(), "messages.yml");
		if (!messagesFile.exists()) {
			messagesFile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}

		messages = new YamlConfiguration();
		try {
			messages.load(messagesFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		/**
		 * Data file
		 */
		dataFile = new File(getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			saveResource("data.yml", false);
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Loa default .yml files
	 */
	void LoadYMLFiles() {
		this.saveDefaultConfig();

		/**
		 * Messages file
		 */
		messagesFile = new File(getDataFolder(), "messages.yml");
		if (!messagesFile.exists()) {
			messagesFile.getParentFile().mkdirs();
			saveResource("messages.yml", false);
		}

		messages = new YamlConfiguration();
		try {
			messages.load(messagesFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
		/**
		 * Data file
		 */
		dataFile = new File(getDataFolder(), "data.yml");
		if (!dataFile.exists()) {
			dataFile.getParentFile().mkdirs();
			saveResource("data.yml", false);
		}

		data = new YamlConfiguration();
		try {
			data.load(dataFile);
		} catch (IOException | InvalidConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Save messages file
	 */
	public void saveMessages() {
		try {
			messages.save(messagesFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Save data file
	 */
	public void saveData() {
		try {
			data.save(dataFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Get Messages config
	 * 
	 * @return
	 */
	public FileConfiguration getMessages() {
		return this.messages;
	}

	/**
	 * Get Data config
	 * 
	 * @return
	 */
	public FileConfiguration getData() {
		return this.data;
	}

	/**
	 * Register Event Listener classes
	 */
	void RegisterListeners() {
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);
	}

	/**
	 * Register command executors
	 */
	void RegisterCommands() {
		getCommand("ontime").setExecutor(new CommandOnTime());
	}
}
