package me.mathiaseklund.stafftracker;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import net.ess3.api.events.AfkStatusChangeEvent;

public class PlayerListener implements Listener {

	Main main = Main.getMain();

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		if (event.getPlayer().hasPermission("ontime.track") || event.getPlayer().isOp()) {
			String uuid = event.getPlayer().getUniqueId().toString();

			long joined = Calendar.getInstance().getTime().getTime();

			int today = Calendar.getInstance().getTime().getDay();
			int month = Calendar.getInstance().getTime().getMonth();

			if (main.getData().get(uuid + ".today") == null) {
				main.getData().set(uuid + ".today", today);
				main.saveData();
			}
			if (main.getData().get(uuid + ".month") == null) {
				main.getData().set(uuid + ".month", month);
				main.saveData();
			}

			if (today != main.getData().getInt(uuid + ".today")) {
				Util.debug("New day.");
				main.getData().set(uuid + ".today", today);
				main.getData().set(uuid + ".playtime.today.total", 0);
				main.getData().set(uuid + ".playtime.today.notafk", 0);
				main.saveData();
			}
			if (month != main.getData().getInt(uuid + ".month")) {
				Util.debug("New month.");
				main.getData().set(uuid + ".month", month);
				main.getData().set(uuid + ".playtime.month.total", 0);
				main.getData().set(uuid + ".playtime.month.notafk", 0);
				main.saveData();
			}

			main.getData().set(uuid + ".joined", joined);
			main.saveData();
		}
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event) {
		if (event.getPlayer().hasPermission("ontime.track") || event.getPlayer().isOp()) {
			long joined = main.getData().getLong(event.getPlayer().getUniqueId().toString() + ".joined");
			long now = Calendar.getInstance().getTime().getTime();
			long playtime = Math.abs(now - joined) / 1000;
			Util.debug(event.getPlayer().getName() + " played for " + playtime);
			int playtime_today_total = main.getData()
					.getInt(event.getPlayer().getUniqueId().toString() + ".playtime.today.total");
			playtime_today_total += playtime;
			int playtime_month_total = main.getData()
					.getInt(event.getPlayer().getUniqueId().toString() + ".playtime.month.total");
			playtime_month_total += playtime;

			main.getData().set(event.getPlayer().getUniqueId().toString() + ".playtime.today.total",
					playtime_today_total);
			main.getData().set(event.getPlayer().getUniqueId().toString() + ".playtime.month.total",
					playtime_month_total);
			main.saveData();
			Util.debug("Total playtime: " + playtime_today_total);
			if (!Main.ess.getUser(event.getPlayer()).isAfk()) {
				int playtime_today_notafk = main.getData()
						.getInt(event.getPlayer().getUniqueId().toString() + ".playtime.today.notafk");
				playtime_today_notafk += playtime;
				main.getData().set(event.getPlayer().getUniqueId().toString() + ".playtime.today.notafk",
						playtime_today_notafk);
				int playtime_month_notafk = main.getData()
						.getInt(event.getPlayer().getUniqueId().toString() + ".playtime.month.notafk");
				playtime_month_notafk += playtime;
				main.getData().set(event.getPlayer().getUniqueId().toString() + ".playtime.month.notafk",
						playtime_month_notafk);
				main.saveData();
			}
		}
	}

	@EventHandler
	public void onAfk(AfkStatusChangeEvent event) {
		Player player = Bukkit.getPlayer(event.getAffected().getName());
		if (player.hasPermission("ontime.track") || player.isOp()) {
			Util.debug("AFK Status Changed");
			if (event.getValue()) {
				Util.debug("Player went AFK");
				long joined = main.getData().getLong(player.getUniqueId().toString() + ".joined");
				long now = Calendar.getInstance().getTime().getTime();
				long playtime = Math.abs(now - joined) / 1000;
				Util.debug(player.getName() + " played for " + playtime);

				int playtime_today_notafk = main.getData()
						.getInt(player.getUniqueId().toString() + ".playtime.today.notafk");
				playtime_today_notafk += playtime;
				main.getData().set(player.getUniqueId().toString() + ".playtime.today.notafk", playtime_today_notafk);
				int playtime_month_notafk = main.getData()
						.getInt(player.getUniqueId().toString() + ".playtime.month.notafk");
				playtime_month_notafk += playtime;
				main.getData().set(player.getUniqueId().toString() + ".playtime.month.notafk", playtime_month_notafk);

				int playtime_today_total = main.getData()
						.getInt(player.getUniqueId().toString() + ".playtime.today.total");
				playtime_today_total += playtime;
				main.getData().set(player.getUniqueId().toString() + ".playtime.today.total", playtime_today_total);
				int playtime_month_total = main.getData()
						.getInt(player.getUniqueId().toString() + ".playtime.month.total");
				playtime_month_total += playtime;
				main.getData().set(player.getUniqueId().toString() + ".playtime.month.total", playtime_month_total);

				main.getData().set(player.getUniqueId().toString() + ".joined", now);
				main.saveData();
				Util.debug("Total playtime: " + playtime_month_total);

			} else {
				Util.debug("Player not AFK anymore");
				long joined = main.getData().getLong(player.getUniqueId().toString() + ".joined");
				long now = Calendar.getInstance().getTime().getTime();
				long playtime = Math.abs(now - joined) / 1000;
				Util.debug(player.getName() + " played for " + playtime);

				int playtime_today_total = main.getData()
						.getInt(player.getUniqueId().toString() + ".playtime.today.total");
				playtime_today_total += playtime;
				main.getData().set(player.getUniqueId().toString() + ".playtime.today.total", playtime_today_total);
				int playtime_month_total = main.getData()
						.getInt(player.getUniqueId().toString() + ".playtime.month.total");
				playtime_month_total += playtime;
				main.getData().set(player.getUniqueId().toString() + ".playtime.month.total", playtime_month_total);

				main.getData().set(player.getUniqueId().toString() + ".joined", now);
				main.saveData();
				Util.debug("Total playtime: " + playtime_month_total);
			}
		}
	}
}
